from django.urls import path
from .views import IndexView,detail,results,vote
app_name = "polls"
urlpatterns = [
    # ex: /polls/
    path("", IndexView.as_view(), name="index"),
    # ex: /polls/5/
    path("specifics/<int:question_id>/", detail, name="detail"),
    # ex: /polls/5/results/
    path("<int:question_id>/results/", results, name="results"),
    # ex: /polls/5/vote/
    path("<int:question_id>/vote/", vote, name="vote"),
    
]